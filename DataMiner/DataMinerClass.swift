//
//  DataMinerClass.swift
//  DataMiner
//
//  Created by Romain Brunie on 12/04/2020.
//  Copyright © 2020 rb. All rights reserved.
//

//class DataMiner {
//    /// TEMPLATE METHOD: series of steps defining the algorithm
//    func mine(path: String) {
//        let file = openFile(path: "pathFile")
//        let rawData = extractData(file: file)
//        let data = parseData(data: rawData)
//        let analysis = analyzeData(analysis: data)
//        sendReport(analysis: analysis)
//        closeFile(file: file)
//    }
//    
//    // MARK: Blueprint of the functions that will require custom implementations in subclasses
//    
//    func openFile(path: String) -> File {
//        fatalError("Need custom implementation !")
//    }
//    
//    func extractData(file: File) -> RawData {
//        fatalError("Need custom implementation !")
//    }
//
//    func parseData(data: RawData) -> `Data` {
//        fatalError("Need custom implementation !")
//    }
//    
//    func closeFile(file: File) {
//        fatalError("Need custom implementation !")
//    }
//    
//    // MARK: Default implementations
//    
//    final func analyzeData(analysis: `Data`) -> Analysis {
//        print("ℹ️ analyze data")
//        return "analysis"
//    }
//    
//    final func sendReport(analysis: Analysis) {
//        print("ℹ️ send report")
//    }
//}
