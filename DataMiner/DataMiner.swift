//
//  DataMiner.swift
//  DataMiner
//
//  Created by Romain Brunie on 18/04/2020.
//  Copyright © 2020 rb. All rights reserved.
//

class FileDataMiner {
    private let dataMiner: DataMiner
    
    init(dataMiner: DataMiner) {
        self.dataMiner = dataMiner
    }
    
    func templateMethod(path: String) {
        dataMiner.mine(path: path)
    }
}
