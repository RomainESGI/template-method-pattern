//
//  DataMinerProtocol.swift
//  DataMiner
//
//  Created by Romain Brunie on 12/04/2020.
//  Copyright © 2020 rb. All rights reserved.
//

typealias File     = String
typealias RawData  = String
typealias `Data`   = String
typealias Analysis = String

/// Blueprint of the functions that will require custom implementations in subclasses
protocol DataMiner {
    func openFile(path: String) -> File
    func extractData(file: File) -> RawData
    func parseData(data: RawData) -> `Data`
    func closeFile(file: File)
}

extension DataMiner {
    /// TEMPLATE METHOD: series of steps defining the algorithm
    func mine(path: String) {
        let file = openFile(path: "pathFile")
        let rawData = extractData(file: file)
        let data = parseData(data: rawData)
        let analysis = analyzeData(analysis: data)
        sendReport(analysis: analysis)
        closeFile(file: file)
    }
    
    // MARK: Default implementations
    
    func analyzeData(analysis: `Data`) -> Analysis {
        print("ℹ️ analyze data")
        return "analysis"
    }
    
    func sendReport(analysis: Analysis) {
        print("ℹ️ send report")
    }
}
