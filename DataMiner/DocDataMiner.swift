//
//  DocDataMiner.swift
//  DataMiner
//
//  Created by Romain Brunie on 12/04/2020.
//  Copyright © 2020 rb. All rights reserved.
//

// conform to protocol
struct DocDataMiner: DataMiner {
    func openFile(path: String) -> File {
        print("📃️ open Doc File")
        return "Doc file opened"
    }
    
    func extractData(file: File) -> RawData {
        print("📃️ extract Doc data")
        return "Doc raw data extracted"
    }
    
    func parseData(data: RawData) -> `Data` {
        print("📃️ parse Doc data")
        return "Doc data parsed"
    }
    
    func closeFile(file: File) {
        print("📃️ close Doc File")
    }
}

// inherit from class
//class DocDataMiner: DataMiner {
//    override init() {
//        super.init()
//        // Call the template method
//        mine(path: "docFilePath")
//    }
//
//    override func openFile(path: String) -> File {
//        print("⚠️ open Doc File")
//        return "Doc file opened"
//    }
//
//    override func extractData(file: File) -> RawData {
//        print("⚠️ extract Doc data")
//        return "Doc raw data extracted"
//    }
//
//    override func parseData(data: RawData) -> `Data` {
//        print("⚠️ parse Doc data")
//        return "Doc data parsed"
//    }
//
//    override func closeFile(file: File) {
//        print("⚠️ close Doc File")
//    }
//}
