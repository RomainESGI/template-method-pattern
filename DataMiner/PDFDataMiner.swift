//
//  PDFDataMiner.swift
//  DataMiner
//
//  Created by Romain Brunie on 12/04/2020.
//  Copyright © 2020 rb. All rights reserved.
//

// conform to protocol
//class PDFDataMiner: DataMiner {
//    init() {
//        // Call the template method
//        mine(path: "PDFFilePath")
//    }
//    
//    func openFile(path: String) -> File {
//        print("📃️ open PDF File")
//        return "PDF file opened"
//    }
//    
//    func extractData(file: File) -> RawData {
//        print("📃️ extract PDF data")
//        return "PDF raw data extracted"
//    }
//    
//    func parseData(data: RawData) -> `Data` {
//        print("📃️ parse PDF data")
//        return "PDF data parsed"
//    }
//    
//    func closeFile(file: File) {
//        print("📃️ close PDF File")
//    }
//}
