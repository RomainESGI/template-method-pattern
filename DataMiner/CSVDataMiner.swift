//
//  CSVDataMiner.swift
//  DataMiner
//
//  Created by Romain Brunie on 12/04/2020.
//  Copyright © 2020 rb. All rights reserved.
//

// conform to protocol
class CSVDataMiner: DataMiner {
    init(path: String) {
        // Call the template method
        mine(path: path)
    }
    
    func openFile(path: String) -> File {
        print("📃️ open CSV File")
        return "CSV file opened"
    }
    
    func extractData(file: File) -> RawData {
        print("📃️ extract CSV data")
        return "CSV raw data extracted"
    }
    
    func parseData(data: RawData) -> `Data` {
        print("📃️ parse CSV data")
        return "CSV data parsed"
    }
    
    func closeFile(file: File) {
        print("📃️ close CSV File")
    }
}
