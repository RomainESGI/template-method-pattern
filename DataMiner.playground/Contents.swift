typealias File     = String
typealias RawData  = String
typealias `Data`   = String
typealias Analysis = String

/// Blueprint of the functions that will require custom implementations in subclasses
protocol DataMiner {
    func openFile(path: String) -> File
    func extractData(file: File) -> RawData
    func parseData(data: RawData) -> `Data`
    func closeFile(file: File)
}

extension DataMiner {
    /// TEMPLATE METHOD: series of steps defining the algorithm
    func mine(path: String) {
        let file = openFile(path: "pathFile")
        let rawData = extractData(file: file)
        let data = parseData(data: rawData)
        let analysis = analyzeData(analysis: data)
        sendReport(analysis: analysis)
        closeFile(file: file)
    }
    
    // MARK: Default implementations
    
    func analyzeData(analysis: `Data`) -> Analysis {
        print("ℹ️ analyze data")
        return "analysis"
    }
    
    func sendReport(analysis: Analysis) {
        print("ℹ️ send report")
    }
}

// DocDataMiner conforms to the DataMiner protocol
class DocDataMiner: DataMiner {
    init() {
        // Call the template method
        mine(path: "docFilePath")
    }
    
    func openFile(path: String) -> File {
        print("⚠️ open Doc File")
        return "Doc file opened"
    }
    
    func extractData(file: File) -> RawData {
        print("⚠️ extract Doc data")
        return "Doc raw data extracted"
    }
    
    func parseData(data: RawData) -> `Data` {
        print("⚠️ parse Doc data")
        return "Doc data parsed"
    }
    
    func closeFile(file: File) {
        print("⚠️ close Doc File")
    }
}

// Testing the Doc implementation of the data miner app
_ = DocDataMiner()
