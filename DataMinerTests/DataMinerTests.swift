//
//  DataMinerTests.swift
//  DataMinerTests
//
//  Created by Romain Brunie on 12/04/2020.
//  Copyright © 2020 rb. All rights reserved.
//

import XCTest
@testable import DataMiner

class DataMinerTests: XCTestCase {
    func testDocDataMiner() {
        // I inject the DocDataMiner to my component taking care of data mining files
        let docDataMiner = FileDataMiner(dataMiner: DocDataMiner())
        docDataMiner.templateMethod(path: "docFilePath")
    }
    
    func testCSVDataMiner() {
        // I simply test a data mining component conforming/inheriting to the DataMiner protocol
        _ = CSVDataMiner(path: "csvFilePath")
    }
//    
//    func testPDFDataMiner() {
//        _ = PDFDataMiner()
//    }
}
