# TEMPLATE METHOD Pattern #

This is the full implementation of the TEMPLATE METHOD pattern for a data mining application.

### How do I get set up? ###

* Project: Select the "DataMinerTests" scheme. Then, you can test (⌘U or Product -> Test) the code.
* Playground: Click the Play button on the last line of code.

